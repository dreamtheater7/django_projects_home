from django.shortcuts import render, redirect
from django.db.models import Q
from django.views import View
from .models import Room, Topic
from .forms import RoomForm


# def home(request):
#     q = request.GET.get('q') if request.GET.get('q') != None else ''
#
#     rooms = Room.objects.filter(
#         Q(topic__name__icontains=q) |
#         Q(name__icontains=q) |
#         Q(description__icontains=q)
#     )
#
#     topics = Topic.objects.all()
#     room_count = rooms.count()
#
#     context = {'rooms': rooms, 'topics': topics, 'room_count': room_count}
#     return render(request, 'base/home.html', context)


class HomeView(View):
    template_name = 'base/home.html'

    def get(self, request):
        q = request.GET.get('q') if request.GET.get('q') != None else ''

        rooms = Room.objects.filter(
            Q(topic__name__icontains=q) |
            Q(name__icontains=q) |
            Q(description__icontains=q)
        )

        topics = Topic.objects.all()
        room_count = rooms.count()

        context = {
            'rooms': rooms,
            'topics': topics,
            'room_count': room_count
        }

        return render(request, 'base/home.html', context)



# def room(request, pk):
#     room = Room.objects.get(id=pk)
#
#     context = {'room': room}
#     return render(request, 'base/room.html', context)

class RoomView(View):
    template_name = 'base/room.html'
    def get(self, request, pk):
        room = Room.objects.get(id=pk)
        context = {'room': room}
        return render(request, 'base/room.html', context)



def createRoom(request):
    form = RoomForm()

    if request.method == "POST":
        form = RoomForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')

    context = {'form': form}
    return render(request, 'base/room_form.html', context)

# class CreateRoomView(View):
#     def post(self, request):
#         form = RoomForm()
#         if request.method == 'POST':
#             form = RoomForm(request.POST)
#             if form.is_valid():
#                 form.save()
#                 return redirect('home')
#
#         context = {'form': form}
#         return render(request, 'base/room_form.html', context)



def updateRoom(request, pk):
    room = Room.objects.get(id=pk)
    form = RoomForm(instance=room)

    if request.method == "POST":
        form = RoomForm(request.POST, instance=room)
        if form.is_valid():
            form.save()
            return redirect('home')


    context = {'form': form}
    return render(request, 'base/room_form.html', context)


def deleteRoom(request, pk):
    room = Room.objects.get(id=pk)

    if request.method == "POST":
        room.delete()
        return redirect('home')

    context = {'obj': room}
    return render(request, 'base/delete.html', context)