from django.urls import path
from .views import HomeView
from . import views

urlpatterns = [
    path('', views.HomeView.as_view(), name="home"),
    # path('room/<str:pk>/', views.room, name="room"),
    path('room/<str:pk>',  views.RoomView.as_view(), name="room"),
    path('create_room/', views.createRoom, name="create_room"),
    path('update_room/<str:pk>/', views.updateRoom, name="update_room"),
    path('delete_room/<str:pk>/', views.deleteRoom, name="delete_room"),
]
