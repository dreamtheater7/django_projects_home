from django.urls import path
from . import views

app_name = 'base'


urlpatterns = [
    path('login/', views.loginPage, name='login'),
    path('logout/', views.logoutUser, name='logout'),
    path('register/', views.registerPage, name='register'),
    # '' in path because it is the root
    path('', views.home, name="home"),
    path('', views.HomeTemplateView.as_view(), name="home"),
    # 'room/' in path because it is localhost/room
    path('room/<str:pk>/', views.room, name="room"),
    path('profile/<str:pk>/', views.userProfil, name="user-profile"),

    path('create-room/', views.createRoom, name="create-room"),
    path('update-room/<str:pk>/', views.updateRoom, name="update-room"),
    path('delete-room/<str:pk>/', views.deleteRoom, name="delete-room"),
    path('delete-message/<str:pk>/', views.deleteMessage, name="delete-message"),

    path('update-user/', views.updateUser, name='update-user'),
    path('topics/', views.topicsPage, name='topics'),
    path('activity/', views.activityPage, name='activity'),
]
