from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import User
# Django Signals


@receiver(post_save, sender=AbstractUser)
def create_user(sender, instance, created, **kwargs):

    if created:
        User.objects.create(user=instance)
        print("User created")

    # pass


#post_save.connect(create_user, sender=User)

@receiver(post_save, sender=AbstractUser)
def update_user(sender, instance, created, **kwargs):
    if not created:
        instance.profile.save()
        print("Profile updated")


#post_save.connect(update_user, sender=User)
