from django.contrib import admin

from .models import User, Topic, Room, Message


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'password',
        'last_login',
        'is_superuser',
        'username',
        'first_name',
        'last_name',
        'is_staff',
        'is_active',
        'date_joined',
        'name',
        'email',
        'bio',
        'avatar',
    )
    list_filter = (
        'last_login',
        'is_superuser',
        'is_staff',
        'is_active',
        'date_joined',
    )
    raw_id_fields = ('groups', 'user_permissions')
    search_fields = ('name',)


@admin.register(Topic)
class TopicAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ('name',)


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'host',
        'topic',
        'name',
        'description',
        'updated',
        'created',
    )
    list_filter = ('host', 'topic', 'updated', 'created')
    raw_id_fields = ('participants',)
    search_fields = ('name',)


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'room', 'body', 'updated', 'created')
    list_filter = ('user', 'room', 'updated', 'created')
