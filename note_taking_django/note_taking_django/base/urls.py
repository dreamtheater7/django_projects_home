
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    # path('', views.home, name="home"),
    # path('room/', views.room, name="room"),
    path('', views.TaskList.as_view(), name="tasks"),
    path('<str:pk>/', views.TaskDetail.as_view(), name="task"),
    path('<str:pk>/', views.TaskDelete.as_view(), name="delete"),
]
