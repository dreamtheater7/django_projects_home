#!/usr/bin/env python3
# -*-coding;cp1252-*-


name = "Johann Wolfgang von Goethe"
def print_hi(fnc_name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {fnc_name}')  # Press Ctrl+F8 to toggle the breakpoint.



class CustomContextManager:

    def __enter__(self):
        #self.open()
        print(f"Opening {name}")
        print_hi(name)

        return name


    def __exit__(self, exc_type, exc_value, exc_tb):
        #self.close()
        print(f'Closing {name} ')
        # handle exception, if necessary

    def open(self):
        pass
        # open the actual connection

    def close(self):
        pass
        # close connection

    def check_if_exists(self, value) -> bool:
        ...

    def _create(self, name):
        ...

    def create(self, name):
        if self.check_if_exists(name):
            return

        self._create(name)

    def exists_firewall(self, identifier: str):
        self.open_searchbar()
        self.search_for(identifier)




# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    with CustomContextManager() as ccm:
        print("this is just a program.")
        ccm.create("name")
    print("done")

    #print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
