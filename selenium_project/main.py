#!/usr/bin/env python3
# -*-coding:cp1252-*-
import sys
import os
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
import time


def main():
    print("", sys.argv)

    options = Options()
    # this is for not automatically closing the browser window
    options.add_experimental_option("detach", True)
    # this is for bypassing the error "Failed to read descriptor"
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    # telling selenium which browser version in use
    os.environ['PATH'] += r"C:/Users/lmoschos/selenium_drivers"
    driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()),
                              options=options)
    driver.get(
        "https://64cfc47c30bde65ff5e99917--amazing-pasca-446bc3.netlify.app/")
    # driver.implicitly_wait(3)
    # # my_element = driver.find_element(By.LINK_TEXT, "Download Python 3.11.4")
    # my_element = driver.find_element(By.TAG_NAME, 'div')
    # my_element.click()
    # progress_element = driver.find_element(By.CLASS_NAME, 'button-text')
    # print(f"{progress_element.text}")
    # time.sleep(30)
    # print(f"{progress_element.text}")

    driver.get(
        "https://www.dts.de/el/omilos-dts/omada/dts-cloud-security-monepe-athina")
    # driver.quit()


if __name__ == "__main__":
    main()
