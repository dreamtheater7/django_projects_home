from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Room(models.Model):
    class Meta:
        ordering = ['-updated', '-created']

    # host = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    # topic = models.ForeignKey(Topic, on_delete=models.SET_NULL, null=True)
    name = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True)
    # participants = models.ManyToManyField(
    #     User, related_name='participants', blank=True)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

