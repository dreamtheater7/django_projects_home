from django.shortcuts import render
from django.views import View
from django.http import HttpResponse

# Create your views here.

rooms = [
    {'id': 1, 'name': 'Python'},
    {'id': 2, 'name': 'Django'},
    {'id': 3, 'name': 'Docker'},
]


class HomeView(View):
    def get(self, request):

        context = {'rooms': rooms}
        return render(request, "base/home.html", context)

    def post(self):
        pass

class RoomView(View):

    def get(self, request, pk):
        room = None

        for i in rooms:
            if i['id'] == int(pk):
                room = i

        context = {'room': room}
        return render(request, "base/room.html", context)

    def post(self):
        pass