#!/usr/bin/env python3
# -*-coding:cp1252-*-
<<<<<<< HEAD
# fastapi
import sys
=======
>>>>>>> 0819dc30d874e7d02ddf4bb8386163581a191c09
from fastapi import FastAPI, Response, status, HTTPException
from fastapi.params import Body
from pydantic import BaseModel
from typing import Optional
from random import randrange


print("", sys.argv)

=======
>>>>>>> 0819dc30d874e7d02ddf4bb8386163581a191c09
app = FastAPI()


class Post(BaseModel):
    title: str
    content: str
    published: bool = True
<<<<<<< HEAD
=======
    # if the user doesn't provide published
    # then it is by default True - otherwise bool
>>>>>>> 0819dc30d874e7d02ddf4bb8386163581a191c09
    rating: Optional[int] = None


my_posts = [{'title': 'title of post 1', 'content': 'content of post 1', 'id': 1},
<<<<<<< HEAD
            {'title': 'favorite food', 'content': 'I like Pizza.', 'id': 2}]

# returns the post in the list with the matching id
# meaning that if id == 1 than return post on postion [0]
=======
            {'title': 'favorite food', 'content': 'I like Pizza.', 'id': 2}, {'title': 'favorite guitars', 'content': 'I like guitars.', 'id': 3}]
>>>>>>> 0819dc30d874e7d02ddf4bb8386163581a191c09


def find_post(id):
    for item in my_posts:
        if id == item['id']:
            return item
<<<<<<< HEAD

# returns the position of the post in the list of dictionaries
# meaning the [0] is the post with id: 1 and
# the [1] is the post with id: 2


def find_index_posts(id):
    for i, p in enumerate(my_posts):
        if p['id'] == id:
            return i

    return 0


@app.get("/")
def root():
    # getting the root localhost
    return {"message": "Welcome to the fastapi!"}
=======
    return 0


def index_post_id(id):
    for i, p in enumerate(my_posts):
        if p['id'] == id:
            return i
        # else:
        #     return 0


@app.get("/")
# async def root(): => async is a keyword for asychronos functions
def root():
    """
    async is not really needed here
    you can name the function what you want -
    but it is good to be as descriptive as posible
    meaning => if you want to create users than a good name would
    be "create_user".

    """
    return {"message": "Welcome to the api!"}
>>>>>>> 0819dc30d874e7d02ddf4bb8386163581a191c09


@app.get("/posts")
def get_posts():
<<<<<<< HEAD
    # getting all the data (posts)
    # return {"data": "This is a list of posts."}
    return {'data': my_posts}


@app.post("/posts")
# def create_post(payload: dict = Body(...)):
def create_post(post: Post):
    # creating a post
=======
    # return {"data": "This is a list of posts."}

    return {'data': my_posts}


@app.post("/posts", status_code=status.HTTP_201_CREATED)
# def create_post(payload: dict = Body(...)):
def create_post(post: Post):
    # def create_post():
    # print(post)
    # print(post.dict())
>>>>>>> 0819dc30d874e7d02ddf4bb8386163581a191c09
    post_dict = post.dict()
    post_dict['id'] = randrange(1, 10000000)
    my_posts.append(post_dict)
    # return {"message": "succesfully created post."}
<<<<<<< HEAD
    return {'data': post_dict}


@app.get("/posts/{id}")
def get_post(id: int, response: Response, post: Post):
    # get one specific post by id
    post = find_post(int(id))
    if not post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
=======
    # return {"new_post": f"title: {payload['title']}, content: {payload['content']}"}
    # return {'data': 'new_post'}
    return {'data': post_dict}


@app.get("/get_posts/{id}")
def get_post(id: int, response: Response):
    # print(id)
    post = find_post(int(id))
    if not post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
        # post = "ID not found."
        # response.status_code = status.HTTP_404_NOT_FOUND
        # return {"message": f"the id {id} not found."}
>>>>>>> 0819dc30d874e7d02ddf4bb8386163581a191c09
    # return {"data": f"get a single post for {id}."}
    return {'data': post}


<<<<<<< HEAD
@app.put("/posts/{id}")
def update_post(id: int, post: Post):

    index = find_index_posts(int(id))
    if index == None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"the post with id {id} was not found.")

    post_dict = post.dict()
    post_dict['id'] = id
    my_posts[index] = post_dict
    return {'data': 'updated succesufully.'}


@app.delete("/posts/{id}")
def delete_post(id: int, post: Post):

    index = find_index_posts(int(id))
    if index == :
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"the post with id {id} was not found.")

    my_posts.pop(index)

    return {'data': 'post deleted'}
=======
@app.delete('/delete_post/{id}', status_code=status.HTTP_204_NO_CONTENT)
def delete_post(id: int, response: Response):
    index = index_post_id(id)

    if index == None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"post with id {id} does not exist.")

    my_posts.pop(index)
    # if not post:
    #     raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)

    # for item in my_posts:
    #     if item['id'] == id:
    #         my_posts.pop(id)

    # return {"data": f"this post with {id} has been deleted."}
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@app.put('/update_posts/{id}')
def post_update(id: int, post: Post):

    print("update post")
    # post = find_post(int(id))
    # if not post:
    #     raise HTTPException(status_code = status.HTTP_404_NOT_FOUND)
    index = index_post_id(int(id))

    if index == None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"post with id {id} does not exist.")

    # print(post)

    post_dict = post.dict()
    print(post_dict)
    post_dict['id'] = id
    my_posts[index] = post_dict

    print("mpolamp")
    return {'data': 'my_posts'}
>>>>>>> 0819dc30d874e7d02ddf4bb8386163581a191c09
