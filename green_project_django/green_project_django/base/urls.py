
from django.urls import path
from . import views

urlpatterns = [
    path('', views.HomeView.as_view(), name="home"),
    path('room/<str:pk>/', views.RoomView.as_view(), name="room"),
    path('create_room/', views.CreateRoomView.as_view(), name="create_room"),
    path('update_room/<str:pk>/', views.UpdateRoomView.as_view(), name="update_room"),
    path('delete_room/<str:pk>', views.RoomDelete.as_view(), name="delete_room"),
]
