from django.shortcuts import render, redirect
from django.views import View
from django.db.models import Q
from .models import Room, Topic, Message
from .forms import RoomForm
from django.http import HttpResponse

# Create your views here.

class HomeView(View):
    def get(self, request):
        q = request.GET.get('q') if request.GET.get('q') != None else ''

        rooms = Room.objects.filter(
            Q(topic__name__icontains=q) |
            Q(name__icontains=q) |
            Q(description__icontains=q)
        )

        topics = Topic.objects.all()
        room_count = rooms.count()
        # rooms = Room.objects.all()
        context = {'rooms': rooms, 'topics': topics, 'room_count': room_count}
        return render(request, "base/home.html", context)


class RoomView(View):
    def get(self, request, pk):
        room = Room.objects.get(id=pk)

        room_messages = room.message_set.all().order_by('-created')
        context = {'room': room, 'room_messages': room_messages}
        return render(request, "base/room.html", context)

    def post(self, request, pk):
        room = Room.objects.get(id=pk)


        if request.method == 'POST':
            message = Message.objects.create(
                user=request.user,
                room=room,
                body=request.POST.get('body')
            )
            return redirect('room', pk=room.id)
        context = {}
        return render(request, "base/room.html", context)


class CreateRoomView(View):
    def get(self, request):
        form = RoomForm
        context = {'form': form}
        return render(request, 'base/room_form.html', context)

    def post(self, request):
        if request.method == "POST":
            form = RoomForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect("home")


class UpdateRoomView(View):
    def get(self, request, pk):
        room = Room.objects.get(id=pk)
        form = RoomForm(instance=room)

        context = {'form': form}
        return render(request, 'base/room_form.html', context)

    def post(self, request, pk):
        room = Room.objects.get(id=pk)

        if request.method == "POST":
            form = RoomForm(request.POST, instance=room)
            if form.is_valid():
                form.save()
                return redirect("home")


class RoomDelete(View):
    def get(self, request, pk):
        room = Room.objects.get(id=pk)

        context = {'obj': room}
        return render(request, 'base/delete.html', context)

    def post(self, request, pk):
        room = Room.objects.get(id=pk)
        if request.method == "POST":
            room.delete()
            return redirect('home')

