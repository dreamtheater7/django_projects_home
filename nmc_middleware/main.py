#!/usr/bin/env python3
# -*-coding:cp1252-*-
import os
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from bs4 import BeautifulSoup
import credents
import time
import re


import sys

print("", sys.argv)

search_word = 'Firewall'

# class NoResultException(ValueError):
#     #print("the value is not an integer.")
#     """ in case of no results. """
#     pass


# class MultiResultException(ValueError):
#     """ in case of multiple results. """
#     #__module__ = Exception.__module__
#     pass


# webdriver setup
options = Options()
options.add_experimental_option("detach", True)
options.add_experimental_option('excludeSwitches', ['enable-logging'])
os.environ['PATH'] += r'C:/Users/lmoschos/selenium_drivers'
driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()),
                          options=options)
driver.get('https://nmc.dts.de/index.php?i=networks&mode=network&n=993')


# login to the website
usrn = driver.find_element(By.ID, 'username')
usrn.send_keys(credents.usrn)
psw = driver.find_element(By.ID, 'password')
psw.send_keys(credents.psw)
submit = driver.find_element(By.TAG_NAME, 'button')
submit.click()

# try:
#     # load the site to be sure that we are on the right page
#     driver.get('https://nmc.dts.de/index.php?i=networks&mode=network&n=993')
#     #assert isinstance(expectedUrl, url), 'Invalid url'
# except:
#     print("right page not found.")
driver.get('https://nmc.dts.de/index.php?i=networks&mode=network&n=993')

actions = ActionChains(driver)

# this should be a function
ActionChains(driver)\
    .send_keys("f")\
    .perform()

# this should be a function
ActionChains(driver)\
    .send_keys(search_word)\
    .perform()


# by pass the spinner
time.sleep(15)
# driver.implicitly_wait(15)
# wait = WebDriverWait(driver, 15)


element_class = driver.find_element(By.CSS_SELECTOR, "div.search-results")
next_element_class = element_class.find_element(By.CSS_SELECTOR, "div.row")
next_next_element_class = next_element_class.find_element(
    By.CSS_SELECTOR, "div.results")
# spinner = next_next_element_class.find_element(By.TAG_NAME, 'i')
# print(spinner.text)
raw_data = next_next_element_class.get_attribute("innerHTML")
print(raw_data)

# using BS4 to parse the inner html content
# soup = BeautifulSoup(raw_data, "html.parser")
# text = soup.get_text()
# elem = soup.findAll('li', {'data-sugtext': 'Security'})
# target_text = elem[0].text

# search_word = 'Firewall'
x_path = f"//li[@data-sugtext='{search_word}']"
# x_path = x_path.replace('Firewall', search_word)
element_by_xpath = next_next_element_class.find_element(
    By.XPATH, x_path)

raw_value = element_by_xpath.text
print(raw_value)

# if isinstance(raw_value, str()):
#     raw_value = int(raw_value)

# text_value = ""  # getting the value from the web element
# for i in target_text:
#     #v = chr(i)
#     if i.isdigit():
#         text_value += i

value_as_str = ""
number_of_devices = 0
if isinstance(raw_value, str):
    for i in raw_value:
        if i.isdigit():
            value_as_str += i
    number_of_devices = int(value_as_str)
else:
    number_of_devices = int(raw_value)


print(f"number_of_devices is {number_of_devices}")

# x = list()
# x = " ".join(re.findall(r"[+-]?\d+(?:\.\d+)?", raw_value))
# x = x.split()
# print(x)

# x = int(" ".join(re.findall(
#     r"\([+-]?\d+(?:\.\d+)?", raw_value)).removeprefix("("))
# print(x)

y = " ".join(re.findall(r"\([+-]?\d+(?:\.\d+)?", raw_value))[1:]
print(y)


# x = " ".join(x)
# print(x)
#
# print(x.removeprefix("("))


# if len(x) > 1:
#     raise Exception("Too many values.")
# else:
#     x = abs(int(" ".join(x)))


# print(x)

# def check_results(text_value):
#     text_value = int(text_value)
#     print(text_value)

#     if text_value == 0:
#         raise NoResultException("No device found.")
#     elif text_value > 1:
#         raise MultiResultException("To many devices found - canot proceed.")
#     else:
#         ActionChains(driver)\
#             .send_keys(Keys.RETURN)\
#             .perform()


# check_results(text_value)
# print(text_value)

driver.quit()
